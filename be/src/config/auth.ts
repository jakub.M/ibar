import { IsNotEmpty } from 'class-validator';

export class Auth {
  @IsNotEmpty({
    message: 'SALT has to be specified',
  })
  SALT = process.env.SALT;

  @IsNotEmpty({
    message: 'JWT_SECRET_KEY has to be specified',
  })
  JWT_SECRET_KEY = process.env.JWT_SECRET_KEY;
}

// export const AUTH = new Auth();

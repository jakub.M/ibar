import { INestApplication } from '@nestjs/common';
import { createApp } from '@utils/test/create-app';

export const initTest = async (call: any): Promise<INestApplication> => {
  const app = await createApp();
  call.setDetails(app);
  return app;
};

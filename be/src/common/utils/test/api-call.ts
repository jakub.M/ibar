import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';

type ReqMethod = 'get' | 'post' | 'delete' | 'put' | 'patch';
type PathFn = (...args: (string | number)[]) => string;

const call = <T extends PathFn>(method: ReqMethod, path: T | string) => {
  const call = (
    token: string = call.token,
    ...params: Parameters<T>
  ): request.Test => {
    const methodPath = fixPath(
      typeof path === 'string' ? path : path(...params),
    );
    return request(call.server)
      [method](methodPath)
      .set('Authorization', `Bearer ${token}`);
  };

  call.server = null;
  call.token = null;
  call.setDetails = (app: INestApplication, token = '') => {
    call.server = app.getHttpServer();
    call.token = token;
  };
  return call;
};

const fixPath = (path) => {
  if (path[0] !== '/') {
    return '/' + path;
  }
  return path;
};

export const callGet = <T extends PathFn>(path: T | string) =>
  call('get', path);
export const callPost = <T extends PathFn>(path: T | string) =>
  call('post', path);
export const callPatch = <T extends PathFn>(path: T | string) =>
  call('patch', path);
export const callPut = <T extends PathFn>(path: T | string) =>
  call('put', path);
export const callDelete = <T extends PathFn>(path: T | string) =>
  call('delete', path);

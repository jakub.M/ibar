export const check = <T = any, R extends T = T>(obj: R) => {
  const check = {
    toOnlyHaveProperty: (...propNames: (keyof T)[]) => {
      propNames.forEach((propName) => expect(obj[propName]).toBeDefined());
      expect(Object.keys(obj).length).toBe(propNames.length);
    },
  };
  return check;
};

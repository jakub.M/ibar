import { PrismaClient } from '@prisma/client';

module.exports = async () => {
  const prisma = new PrismaClient();
  const models = Reflect.ownKeys(prisma).filter((key) => key[0] !== '_');
  const upperFirst = (name) => name.charAt(0).toUpperCase() + name.slice(1);
  await prisma.$transaction([
    ...models
      .map((modelKey) => upperFirst(modelKey))
      .map((tableName) => prisma.$queryRaw('DELETE FROM `' + tableName + '`;')),
  ]);
};

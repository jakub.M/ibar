import * as crypto from 'crypto';

export const hash = (p: string): string =>
  crypto.createHmac('sha256', process.env.SALT).update(p).digest('hex');

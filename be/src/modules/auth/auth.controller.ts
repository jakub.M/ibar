import { LoginCommand } from '@modules/auth/commands/impl/login.command';
import { LoginDto } from '@modules/auth/login.dto';
import { Body, Controller, Post } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';

@Controller('auth')
export class AuthController {
  constructor(private commandBus: CommandBus) {}

  @Post('login')
  login(@Body() loginData: LoginDto) {
    return this.commandBus.execute(new LoginCommand(loginData));
  }
}

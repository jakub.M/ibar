import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { PrismaService } from '@prism-service';
import { User } from '@prisma/client';

@Injectable()
export class AuthService {
  constructor(private prisma: PrismaService, private jwtService: JwtService) {}

  createAccessToken(user: User) {
    const { id: sub, role, email } = user;
    const accessToken = this.jwtService.sign({
      email,
      sub,
      role,
    });

    return accessToken;
  }
}

import * as faker from 'faker';
import { hash } from '@utils/hash';
import { callPost } from '@utils/test/api-call';
import { initTest } from '@utils/test/init-test';
import { check } from '@utils/test/check';
import { PrismaService } from '@config/prisma.service';
import { INestApplication } from '@nestjs/common';
import { Role } from '@prisma/client';

describe('POST: auth/login', () => {
  let app: INestApplication;
  let prisma: PrismaService;

  const createLoginData = () => ({
    email: faker.internet.email(),
    password: faker.internet.password(8),
  });

  const loginData = createLoginData();

  const createUser = async () =>
    await prisma.user.create({
      data: {
        email: loginData.email,
        password: hash(loginData.password),
        role: Role.CUSTOMER,
      },
    });

  const callLogin = callPost('auth/login');

  beforeAll(async () => {
    app = await initTest(callLogin);
    prisma = app.get(PrismaService);
    await createUser();
  });

  afterAll(async () => {
    app.close();
  });

  it('Should return 201 and return token', async () => {
    await callLogin()
      .send(loginData)
      .expect(201)
      .expect(({ body }) => {
        check(body).toOnlyHaveProperty('accessToken');
      });
  });

  it('Should throw 404 for invalid login data', async () => {
    await callLogin()
      .send(createLoginData())
      .expect(401)
      .expect(({ body }) => {
        expect(body.message).toBe('Invalid email or password');
      });
  });
});

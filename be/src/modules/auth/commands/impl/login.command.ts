import { LoginDto } from '@modules/auth/login.dto';

export class LoginCommand {
  constructor(public readonly loginData: LoginDto) {}
}

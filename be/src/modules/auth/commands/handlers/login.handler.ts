import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { LoginCommand } from '@modules/auth/commands/impl/login.command';
import { UsersService } from '@modules/users/users.service';
import { AuthService } from '@modules/auth/auth.service';

@CommandHandler(LoginCommand)
export class LoginHandler implements ICommandHandler<LoginCommand> {
  constructor(
    private userService: UsersService,
    private authService: AuthService,
  ) {}

  async execute({ loginData: { email, password } }: LoginCommand) {
    const user = await this.userService.findAuthUser(email, password);
    const accessToken = this.authService.createAccessToken(user);
    return { accessToken };
  }
}

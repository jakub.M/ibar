import { AuthModule } from '@modules/auth/auth.module';
import { PlacesModule } from '@modules/places/places.module';
import { UsersModule } from '@modules/users/users.module';
import { Module } from '@nestjs/common';

@Module({
  imports: [AuthModule, UsersModule, PlacesModule],
})
export class AppModule {}

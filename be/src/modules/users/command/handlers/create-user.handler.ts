import { PrismaService } from '@config/prisma.service';
import { AuthService } from '@modules/auth/auth.service';
import { CreateUserCommand } from '@modules/users/command/impl/create-user.command';
import { CreateUserDto } from '@modules/users/dtos/create-user.dto';
import { UsersService } from '@modules/users/users.service';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Role } from '@prisma/client';
import { hash } from '@utils/hash';

@CommandHandler(CreateUserCommand)
export class CreateUserHandler implements ICommandHandler<CreateUserCommand> {
  constructor(
    private prisma: PrismaService,
    private usersService: UsersService,
    private authService: AuthService,
  ) {}

  async execute({ userData }: CreateUserCommand) {
    await this.usersService.findUniqueUser(userData.email);
    const createdUser = await this.createUserWithPlaceIfNeeded(userData);
    return this.authService.createAccessToken(createdUser);
  }

  private createUserWithPlaceIfNeeded({
    placeName,
    password,
    ...restData
  }: CreateUserDto) {
    return this.prisma.user.create({
      data: {
        ...restData,
        password: hash(password),
        ...(restData.role === Role.OWNER
          ? { places: { create: { name: placeName } } }
          : {}),
      },
    });
  }
}

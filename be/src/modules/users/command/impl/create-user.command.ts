import { CreateUserDto } from '@modules/users/dtos/create-user.dto';

export class CreateUserCommand {
  constructor(public readonly userData: CreateUserDto) {}
}

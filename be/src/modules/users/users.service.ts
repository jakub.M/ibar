import { PrismaService } from '@config/prisma.service';
import {
  ConflictException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { hash } from '@utils/hash';

@Injectable()
export class UsersService {
  constructor(private prisma: PrismaService) {}

  async findAuthUser(email: string, password: string) {
    const user = await this.prisma.user.findFirst({
      where: { email, password: hash(password) },
    });

    if (!user) {
      throw new UnauthorizedException('Invalid email or password');
    }

    return user;
  }

  async findUniqueUser(email: string) {
    const user = await this.prisma.user.findFirst({ where: { email } });

    if (user) {
      throw new ConflictException('User already exists for this email');
    }
  }
}

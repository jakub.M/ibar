import { Match } from '@decorators/match.decorator';
import { LoginDto } from '@modules/auth/login.dto';
import { Role } from '@prisma/client';
import { IsEnum, IsIn, IsString, ValidateIf } from 'class-validator';

export class CreateUserDto extends LoginDto {
  @Match('password')
  passwordConfirmation: string;

  @IsIn([Role.OWNER, Role.CUSTOMER])
  @IsEnum(Role)
  role: Role;

  @ValidateIf(({ role }) => role === Role.OWNER)
  @IsString()
  placeName?: string;
}

## Installation

```bash
$ npm install
```

## Running the DB (with admin panel)

```bash
$ docker-compose up
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run dev

# debug mode
$ npm run debug

# production mode
$ npm run prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# e2e test matched files
$ npm run test:e2e -n ${fileName}
```
## Migrations

```bash
# generate migration and run it
npx prisma migrate dev --name ${migrationName}

#generate migration
npx prisma migrate dev --name ${migrationName} --create-only

# run migration
$ npx prisma migrate dev
```

